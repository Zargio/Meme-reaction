extends CanvasLayer

enum PLAYERS {PLAYER_1, PLAYER_2}

signal start_game
signal undo_signal
signal segnale_esci

export (AudioStream) var suono_bottone
export (AudioStream) var suono_slider

func _ready():
	if get_parent().player == PLAYERS.PLAYER_1:
		$Contenitore_alto/Chi_sta_giocando.bbcode_text = "[center][color=#00ff00]Giocatore 1[/color]"
	else:
		$Contenitore_alto/Chi_sta_giocando.bbcode_text = "[center][color=red]Giocatore 2[/color]"
	$Contenitore_verticale/Schermata_iniziale_testo.text = "Seleziona il numero di colonne\n(%d)" % $Contenitore_verticale/Contenitore_centrante/Input_numero_colonne.value


func _on_Start_button_pressed():
	$SFX.stream = suono_bottone
	$SFX.play()
	emit_signal("start_game")

func _on_Input_numero_colonne_value_changed(value):
	$SFX.stream = suono_slider
	$SFX.play()
	$Contenitore_verticale/Schermata_iniziale_testo.text = "Seleziona il numero di colonne\n(%d)" % value


func _on_Undo_button_pressed():
	if !get_parent().cells_to_change.empty():
		return
	emit_signal("undo_signal")


func _on_Esci_tasto_pressed():
	if !get_parent().cells_to_change.empty():
		return
	$PopupPanel.popup()


func _on_tasto_no_pressed():
	$PopupPanel.hide()


func _on_tasto_ok_pressed():
	$PopupPanel.hide()
	emit_signal("segnale_esci")
