extends PanelContainer

# Each cell has an identification num to be used and set by the game manager
var num:int = -1

# The state indicates the state of the cell, not actually the state. It's because I'm dumb. FIXME -> Refactor name
var state:int = 0

enum PLAYERS {PLAYER_1, PLAYER_2}
var player = PLAYERS.PLAYER_1

# The player who performed an action this turn, gotten from the Game_manager
var current_player

# The max number of state it can contain, or the number of neighbour cells
var neighbours:int  = 4

var game_manager:Node2D

# Tell the game_manager the cell that changed and its state, so that it knows wether it has to increase the nearby ones
signal turn_started(cell_changed)

# When the cell is pressed
func _on_Cella_bottone_pressed():
	if !game_manager.game_finished and game_manager.cells_to_change.empty():
		current_player = game_manager.player
		# If the player can actually change the cell, or the cell doesn't belong to anyone (state == 0), increase it
		if state == 0 or player == current_player:
			emit_signal("turn_started",num)

func increase() -> void:
	# If the cell is increasing, it must belong to the current player
	player = current_player
	state = (state + 1)%neighbours
	update_texture()

const player_1_color = Vector3(0,1,0);
const player_2_color = Vector3(1,0,0);
func update_texture() -> void:
	match player:
		PLAYERS.PLAYER_1:
			$Cella_bottone.material.set_shader_param("player",player_1_color)
		PLAYERS.PLAYER_2:
			$Cella_bottone.material.set_shader_param("player",player_2_color)
	$Cella_bottone.material.set_shader_param("state",state)
