extends Node2D

# To spawn the cells
export(PackedScene) var cell_scene

export(int) var grid_offset = 70
enum PLAYERS {PLAYER_1, PLAYER_2}
var player = PLAYERS.PLAYER_1

# Get window dimensions
var window_dims:Vector2
var cols:int
var rows:int

var stati_precedenti = []
var cells = []
var game_finished: bool = false
var scaling:float
var spacing:float
var cells_to_change = []

# I load them here so that I don't have to load the files each time a new cell is instantiated
const grey_square = preload("res://Assets/Texture/grey square.svg")
var texture_dims = grey_square.get_size()
"""

const grey_square_1g = preload("res://Assets/Texture/grey square(1g).svg")
const grey_square_2g = preload("res://Assets/Texture/grey square(2g).svg")
const grey_square_3g = preload("res://Assets/Texture/grey square(3g).svg")
const grey_square_1r = preload("res://Assets/Texture/grey square(1r).svg")
const grey_square_2r = preload("res://Assets/Texture/grey square(2r).svg")
const grey_square_3r = preload("res://Assets/Texture/grey square(3r).svg")
"""

const suono_cella_rossa = preload("res://Assets/Suoni/cell03.wav")
const suono_cella_verde = preload("res://Assets/Suoni/cell04.wav")

const player_1_color = Vector3(0,1,0);
const player_2_color = Vector3(1,0,0);
# Start the game
func _on_HUD_start_game() -> void:
	# Hide the title screen and show who is playing
	$HUD/Contenitore_verticale.hide()
	$HUD/Contenitore_alto.show()

	# The number of columns comes from the slider in the HUD
	cols = $HUD/Contenitore_verticale/Contenitore_centrante/Input_numero_colonne.value
	# The scaling is the value by which each cell will be scaled, it's divided by 17, because the sprite is 16px, but we add 1 to have some spacing between each cell
	window_dims = get_viewport_rect().size
	scaling = float(window_dims.x) / cols / (texture_dims.x)
	# scaling = window_dims.x / cols
	spacing = scaling * (texture_dims.x + 1)
	# spacing = scaling + 2
	rows = (window_dims.y - grid_offset) / spacing
	# rows = 
	$GridContainer.columns = cols
	$GridContainer.margin_right = window_dims.x
	$GridContainer.margin_bottom = grid_offset + rows * window_dims.x / cols
	
	for i in range(cols * rows):
		var cell = cell_scene.instance()
		$GridContainer.add_child(cell)
		cell.num = i
		cell.game_manager = self
		var tmp = 4
		if i < cols:
			tmp -= 1
		if i % cols == 0:
			tmp -= 1
		if (i + 1) % cols == 0:
			tmp -= 1
		if i > (rows * cols) - cols - 1:
			tmp -= 1
		cell.neighbours = tmp
		
		cells.append(cell)
		cell.connect("turn_started",self,"_on_turn_started")

# Cosa fare quando il giocatore di turno ha cliccato su una cella
func _on_turn_started(cell_changed) -> void:
	# All'inizio di ogni turno, salva lo stato di tutte le celle prima che inizi, cosi si puo tornare indietro
	# Stati inizio turno contiene un array con tutti i valori correnti di [stato, giocatore a cui appartiene] per ogni cella
	var stati_inizio_turno = []
	for i in cells:
		stati_inizio_turno.append([i.state,i.player])
	# Stati precedenti contiene tutti gli stati_inizio_turno passati
	stati_precedenti.append(stati_inizio_turno)
	# Per tenere conto di quali celle cambiare, le metto in un array e quando ho finito il turno size sara 0
	cells_to_change.append(cell_changed)
	while cells_to_change.size() > 0:
		# Ho bisogno di tmp perche devo leggere poi inserire e infine togliere le celle,
		# tmp contiene le celle da cambiare, mentre cells_to_change verra popolato con nuove celle per la prossima iterazione del while
		var tmp = cells_to_change
		cells_to_change = []
		# exploding_cells contiene un array di sprite usati per la animazione quando esplodono le celle
		# Dobbiamo metterli in un array perche cosi una volta che hanno finito la animazione possono essere distrutte
		var exploding_cells = []
		for i in tmp:
			# Aumenta lo stato della cella
			cells[i].current_player = player
			cells[i].increase()
			# Se la cella e esplosa
			if cells[i].state == 0:
				# Controlla i vicini, e aggiungili a quelli che devono aumentare il prossimo turno,
				# Inoltre crea gli sprite per la animazione, collegandoli a Tween
				if player == PLAYERS.PLAYER_1:
					$AudioStreamPlayer.stream = suono_cella_verde
				else:
					$AudioStreamPlayer.stream = suono_cella_rossa
				$AudioStreamPlayer.play()
				
				# Controlla in alto
				if i > cols - 1:
					cells_to_change.append(i - cols)
					add_exploding_cell(exploding_cells,i,0,-1)
				# Controlla a sinistra
				if i % cols != 0:
					cells_to_change.append(i - 1)
					add_exploding_cell(exploding_cells,i,-1,0)
				# Controlla a destra
				if (i + 1) % cols != 0:
					cells_to_change.append(i + 1)
					add_exploding_cell(exploding_cells,i,1,0)
				# Controlla giu
				if i < (rows * cols) - cols:
					cells_to_change.append(i + cols)
					add_exploding_cell(exploding_cells,i,0,1)
		# Fai partire tutte le animazioni insieme e aspetta che finiscano (yield)
		$Tween.start()
		yield($Tween,"tween_all_completed")
		# Elimina gli sprite usati per le animazioni
		for j in exploding_cells:
			j.queue_free()
		exploding_cells.clear()
		# E gli indirizzi di quelli che sono stati appena aumentati
		tmp.clear()
		# Se qualcuno ha vinto
		if check_winner():
			# Fai partire un timer per un piccolo delay prima che si torni alla schermata di partenza, e tutto si resetti
			$Restart_delay.start()
			if player == PLAYERS.PLAYER_1:
				$HUD/Contenitore_verticale/Titolo_del_gioco.text = "Player 1 won"
			else:
				$HUD/Contenitore_verticale/Titolo_del_gioco.text = "Player 2 won"
			game_finished = true
			return
	change_player()

# Cosa fare quando finisce il timer di delay della fine della partita
func _on_Restart_delay_timeout() -> void:
	game_over()

# Cosa fare quando è premuto il bottone undo
func _on_HUD_undo_signal() -> void:
	# Non fare nulla se non si puo tornare indietro, o se il turno non e finito
	if stati_precedenti.empty() or !cells_to_change.empty():
		return
	# Ritorna la griglia allo stato precedente
	change_player()
	var tmp = stati_precedenti.pop_back()
	var j:int = 0
	for i in tmp:
		cells[j].state = i[0]
		cells[j].player = i[1]
		cells[j].update_texture()
		j += 1

# Cosa fare quando è premuto il bottone esci
func _on_HUD_segnale_esci():
	$HUD/Contenitore_verticale/Titolo_del_gioco.text = "Titolo del gioco"
	game_finished = true
	game_over()
	
func check_winner() -> bool:
	if stati_precedenti.size() < 2:
		return false
	for i in cells:
		if i.state > 0 and i.player != player:
			return false
	return true
	
# Funzione per pulire risorse utilizzate dal gioco che si è appena concluso, e mostrare/nascondere la UI
func game_over() -> void:
	cells_to_change.clear()
	player = PLAYERS.PLAYER_1
	stati_precedenti.clear()
	$HUD/Contenitore_alto.hide()
	$HUD/Contenitore_verticale.show()
	for i in cells:
		i.queue_free()
	cells.clear()
	game_finished = false

func change_player() -> void:
	if player == PLAYERS.PLAYER_1:
		player = PLAYERS.PLAYER_2
		$HUD/Contenitore_alto/Chi_sta_giocando.bbcode_text = "[center][color=red]Giocatore 2[/color]"
	else:
		player = PLAYERS.PLAYER_1
		$HUD/Contenitore_alto/Chi_sta_giocando.bbcode_text = "[center][color=#00ff00]Giocatore 1[/color]"
	
	# E se rendessimo il background delle celle vuote del colore del giocatore di turno?
	for i in cells:
		if i.state == 0:
			i.player = player
			i.update_texture()
	
		
# const exploding_green = preload("res://Assets/Texture/simple green.svg")
# const exploding_red = preload("res://Assets/Texture/simple red.svg")
const exploding_cell_material = preload("res://Assets/Shader_material/Exploding_shader_material.tres")
func add_exploding_cell(exploding_cells,i,x_off,y_off) -> void:
	var expl = Sprite.new()
	expl.material = exploding_cell_material
	expl.texture = grey_square
	expl.scale = Vector2(scaling,scaling)
	
	if x_off == -1:
		expl.rotation = 180
	elif y_off == 1:
		expl.rotation = 90
	elif y_off == 1:
		expl.rotation = -90
	
	var size = cells[i].rect_size
	# Di default e verde, quindi non serve riassegmarlo in quel caso 
	var begin_pos = cells[i].rect_position + size / 2 + Vector2(0,grid_offset)
	var end_pos = cells[i + x_off + y_off * cols].rect_position + size / 2 + Vector2(0,grid_offset)
	
	if player == PLAYERS.PLAYER_1:
		expl.material.set_shader_param("player",player_1_color)
		#expl.texture = exploding_green
	else:
		expl.material.set_shader_param("player",player_2_color)
		#expl.texture = exploding_red
	add_child(expl)
	# Passa dalla posizione iniziale della cella n. i a quella vicina
	$Tween.interpolate_property(expl,"position",begin_pos,end_pos,1)
	exploding_cells.append(expl)
