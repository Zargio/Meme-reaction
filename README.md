# Meme Reaction
*Exploding molecules since 2022*

<img src="./screenshots/explosion_gif.gif" alt="Explosion Gif" width="150"/>

## The idea
This game is some sort of remake of other mobile games but in a free and open source version and made with a free and open source game engine (Godot). It takes inspiration from [Chain Reaction](https://play.google.com/store/apps/details?id=com.BuddyMattEnt.ChainReaction), a simple but very cool mobile game.

## Screenshots
![Game Screenshot](./screenshots/tot_screenshot.png)


## Something like a roadmap
### (but more like random ideas - help appreciated!)
- [#2] Timed version
- [#3] Player or match stats
- [#4] More than 2 players
- [#6] Online multiplayer
- Your idea!
